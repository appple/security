from django import forms
from .models import *


class PairingForm(forms.Form):
    name = forms.CharField(max_length=128)
    address = forms.CharField(max_length=128)


class MessageForm(forms.Form):
    class Meta:
        model = Message
        exclude = ()


class UserForm(forms.Form):
    username = forms.CharField(max_length=128)
    password = forms.CharField(widget=forms.PasswordInput())


class ContactNameForm(forms.Form):
    class Meta:
        model = Contact
        fields = ('name',)