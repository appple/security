import requests
from django.core import serializers
from .serializers import *
from .models import *
from .encryptions import *

def verify_user(address, name, password):

    try:
        user = MyUser.objects.get(address=address, name=name)
    except MyUser.DoesNotExist:

        private_key, mod_val, public_key = get_crypto_keys()

        password = MD5(password)

        sys_user = User.objects.create_user(username=name, email='random@rand.com', password=password)

        user = MyUser(
            name=name,
            address=address,
            password=password,
            private_key=private_key,
            mod_val=mod_val,
            public_key=public_key,
            user=sys_user
        )
        user.save()


    return user


def hand_shake(user, address, name):
    url = address + 'handshake/'

    # tmp_address = user.address.replace(['/'],'~')
    data = {
            # 'id': user.id,
            'user': name,
            'name': user.name,
            'address': user.address,
            'public_key': user.public_key,
            'mod_val': user.mod_val
        }

    response = requests.get(url=url, data=data)

    if response.status_code == 200:

        print(response)
        print(response.json())

        contact = Contact(
            user=user.user,
            name=name,
            address=address,
            public_key=response.json()['public_key'],
            mod_val=response.json()['mod_val']
        )

        try:
            contact.save()
            print('pair success')
            return True
        except Exception as e:
            print('pair failed')
            print(e)
            return False

    print('pair failed')
    return False








def send_message(message):
    user = MyUser.objects.get(id=message.sender.id)
    receivers = Contact.objects.filter(id__in=message.receivers.all())
    # data = MessageSerializer(instance=message).data

    # message = data['message']
    # message = private_encrypt(user.private_key, user.private_key_n, message)

    data = {
        'sender_name': user.name,
        'sender_address':user.address,
        'message': message.message,
        'receivers_name': '~'.join([a.name for a in receivers]),
        'receivers_address': '~'.join([a.address for a in receivers]),
        'is_e': message.is_e
    }

    print('SEND MESSAGE FUNCTION')
    print(message)
    print(data)
    response = {}
    mess = data['message']
    for receiver in receivers:
        url = receiver.address + 'receive/'
        data['message'] = private_encrypt(user.private_key, user.mod_val, user.name) \
                          +' ~ '+\
                          public_encrypt(receiver.public_key, receiver.mod_val, mess)
        # data['tag'] = private_encrypt(user.private_key, user.mod_val, data['tag'])
        data['ca'] =receiver.address
        data['cn'] =receiver.name
        response[receiver] = requests.post(url=url, data=data)
    return response

